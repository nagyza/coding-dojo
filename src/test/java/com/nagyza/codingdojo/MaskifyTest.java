package com.nagyza.codingdojo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaskifyTest {

    Maskify underTest = new Maskify();

    @Test
    public void testMaskify() {
        assertEquals("############5616", underTest.maskify("4556364607935616"));
        assertEquals("#######5616",      underTest.maskify(     "64607935616"));
        assertEquals("1",                underTest.maskify(               "1"));
        assertEquals("",                 underTest.maskify(                ""));

        // "What was the name of your first pet?"
        assertEquals("##ippy",                                    underTest.maskify("Skippy")                                  );
        assertEquals("####################################man!",  underTest.maskify("Nananananananananananananananana Batman!"));
    }
}